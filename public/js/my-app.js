// Initialize your app
var myApp = new Framework7({
    swipePanel: 'left'
});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

$$(document).on('click','.projectback',function(){
	//mainView.router.reloadPage();
	
	mainView.router.back({
		'url':'projects.php',
		'animatePages' : true
	});
});

$$(document).on('deleted','.swipeout',function(e){
	if($$(this).attr('data-type') == 'remove_platform')
	{
		platform = $$(this).attr('data-id');
		project = $$(this).attr('data-project');
		
		$.ajax({
			type : "POST",
			url : 'viewproject/removeplatform.php',
			beforeSend: function( xhr ) {
				myApp.showIndicator();
			},
			data: {
				platform: platform,
				projectname: project
			},
			complete: function() {
				myApp.hideIndicator();
			}
		}).done(function(response) {
			
		});
		
	} else if($$(this).attr('data-type') == 'remove_project'){
		project = $$(this).attr('data-project');
		
		$.ajax({
			type : "POST",
			url : 'viewproject/removeproject.php',
			beforeSend: function( xhr ) {
				myApp.showIndicator();
			},
			data: {
				projectname: project
			},
			complete: function() {
				myApp.hideIndicator();
			}
		}).done(function(response) {
			
		});
	}
});

$$('.panel-left .list-block ul li a.item-link').on('click',function(){
	$.ajax({
		type : "POST",
		url : $(this).attr('data-url'),
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(msg) {
		mainView.router.loadContent(msg);
	});
});

$$(document).on('click','#addProject',function(){
	$.ajax({
		type : "POST",
		url : 'addproject/add.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		mainView.router.loadContent(response);
	});
});

//Step2
$$(document).on('click','#addProjectStep2',function(){
	Projectname = $('#project_name').val();
	if(Projectname == '')
	{
		myApp.alert('Please enter project name');
		return;
	} 
	
	$.ajax({
		type : "POST",
		url : 'addproject/createproject.php',
		dataType: 'json',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: { 
			projectname: Projectname
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		
		if(typeof response.Type != 'undefined')
		{
			if(response.Type == 'Success')
			{
				$.ajax({
					type : "POST",
					url : 'addproject/add_step2.php',
					beforeSend: function( xhr ) {
						myApp.showIndicator();
					},
					data: {
						projectname: Projectname, 
					},
					complete: function() {
						myApp.hideIndicator();
					}
				}).done(function(response) {
					localStorage.setItem("currentproject", Projectname);
					mainView.router.loadContent(response);
				});
				
			} else {
				myApp.alert(response.Message);
			}
		}
		console.log(response);
		//mainView.router.loadContent(response);
	});

});

//Step3
$$(document).on('click','#addProjectStep3',function(){
	Projectname = localStorage.getItem('currentproject');
	var storedData = myApp.formGetData('step2form');
	  if(!storedData) {
		  alert('Please select platform');
		  return;
	  }
	  
	
	$.ajax({
		type : "POST",
		url : 'addproject/addplatform.php',
		dataType: 'json',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: { 
			projectname: Projectname,
			platforms: storedData['ks-checkbox']
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		if(typeof response.Type != 'undefined')
		{
			if(response.Type == 'Success')
			{
				$.ajax({
					type : "POST",
					url : 'add_step3.php',
					beforeSend: function( xhr ) {
						myApp.showIndicator();
					},
					data: {
						projectname: Projectname, 
					},
					complete: function() {
						myApp.hideIndicator();
					}
				}).done(function(response) {
					mainView.router.loadContent(response);
					localStorage.removeItem('currentproject');
				});
				
			} else {
				myApp.alert(response.Message);
			}
		}
		console.log(response);
		//mainView.router.loadContent(response);
	});

});

$$(document).on('click','.view_project',function(){
	Projectname = $$(this).attr('data-id');
	viewProject(Projectname);
});

function viewProject(Projectname)
{
	localStorage.setItem("currentproject", Projectname);
	$.ajax({
		type : "POST",
		url : 'viewproject/viewproject.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname, 
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		mainView.router.loadContent(response);
	});
}

$$(document).on('click','#addPlatform',function(){
	Projectname = localStorage.getItem('currentproject');
	$.ajax({
		type : "POST",
		url : 'viewproject/availableplatform.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname, 
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		mainView.router.loadContent(response);
	});
});

$$(document).on('click','#platform_add',function(){
	Projectname = localStorage.getItem('currentproject');
	var storedData = myApp.formGetData('availableplatforms');
	$.ajax({
		type : "POST",
		url : 'viewproject/addplatform.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname, 
			platforms: storedData['ks-checkbox']
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		viewProject(Projectname);
	});
});



$$(document).on('click','.view_platform',function(){
	
	if($(this).data('id').indexOf('android') === -1)
	{
		
	} else {
		Projectname = $(this).data('project');
		$.ajax({
			type : "POST",
			url : 'androidproject/view.php',
			beforeSend: function( xhr ) {
				myApp.showIndicator();
			},
			data: {
				projectname: Projectname
			},
			complete: function() {
				myApp.hideIndicator();
			}
		}).done(function(response) {
			mainView.router.loadContent(response);
		});
		
	}
	
	
});

$$(document).on('click','#generate_android_keystore',function(){
	Projectname = $(this).data('project');
	type = $(this).data('type');
	$.ajax({
		type : "POST",
		url : 'androidproject/generatekeystore.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname,
			type: type
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		
	});
});
$$(document).on('click','#run_android_project',function(){
	Projectname = $(this).data('project');
	$.ajax({
		type : "POST",
		url : 'androidproject/run.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		
	});
});
$$(document).on('click','.projectbuild',function(){
	Projectname = $(this).data('project');
	$.ajax({
		type : "POST",
		url : 'androidproject/build.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname,
			isDownload: false
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		
	});
});
$$(document).on('click','#build_android_download',function(){
	Projectname = $(this).data('project');
	$.ajax({
		type : "POST",
		url : 'androidproject/build.php',
		beforeSend: function( xhr ) {
			myApp.showIndicator();
		},
		data: {
			projectname: Projectname,
			isDownload: true
		},
		complete: function() {
			myApp.hideIndicator();
		}
	}).done(function(response) {
		window.location = 'download.php?projectname='+Projectname;
	});
});
$$(document).on('click','#android_download',function(){
	Projectname = $(this).data('project');
	window.location = 'download.php?projectname='+Projectname;
});



