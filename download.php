<?php
// echo '<pre>';
// print_r($_GET);die;
$projectName = (array_key_exists('projectname', $_GET)) ? $_GET['projectname'] : '';

$installedPlatforms = array();
if ($projectName != '') {
	$downloadFile = realpath(dirname(__FILE__)).'/Projects/'.$projectName.'/platforms/android/ant-build/MainActivity-debug.apk';
	
	downloadFile($downloadFile,$projectName);
}


function downloadFile($fullPath,$filename)
{
	ignore_user_abort(true);
	set_time_limit(0); // disable the time limit for this script

	if ($fd = fopen ($fullPath, "r")) {
		$fsize = filesize($fullPath);
		$path_parts = pathinfo($fullPath);
		$ext = strtolower($path_parts["extension"]);
		$downloadFileName = $filename.'.apk';
		switch ($ext) {
			case "pdf":
				header("Content-type: application/pdf");
				header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a file download
				break;
				// add more headers for other content types here
			default;
			header("Content-type: application/octet-stream");
			header("Content-Disposition: filename=\"".$downloadFileName."\"");
			break;
		}
		header("Content-length: $fsize");
		header("Cache-control: private"); //use this to open files directly
		while(!feof($fd)) {
			$buffer = fread($fd, 2048);
			echo $buffer;
		}
	}
	fclose ($fd);
	exit;
}