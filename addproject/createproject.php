<?php 
include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';

$files1 = scandir('../Projects');

if (trim($projectName) != '') {
	if(count($files1) > 0) {
		foreach ($files1 as $file) {
			if (trim(strtolower($file)) == trim(strtolower($projectName))) {
				echo json_encode(array(
					'Type' => 'Error',
					'Message' => 'Project already exists.'
				));die;
				break;
			}
		}
	}
	
	$cordova = new Cordova($projectName);
	$return = $cordova->createProject();
	
	if ($return) {
		echo json_encode(array(
				'Type' => 'Success',
				'Message' => ''
		));die;
	} else {
		echo json_encode(array(
				'Type' => 'Error',
				'Message' => 'Something goes wrong. Please contact administrator.'
		));die;
	}
	
	
} else {
	echo json_encode(array(
			'Type' => 'Error',
			'Message' => 'Enter proper project name.'
	));die;
}

?>