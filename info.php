<?php 
include 'lib/cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
if ($projectName != '') {
	$cordova = new Cordova($projectName);
}
?>


<!-- We don't need full layout here, because this page will be parsed with Ajax-->
<!-- Top Navbar-->
<div class="navbar">
  <div class="navbar-inner">
    <div class="left"><a href="#" class="back link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Info</div>
    <div class="right">
      <!-- Right link contains only icon - additional "icon-only" class--><a href="#" class="link icon-only open-panel"> <i class="icon icon-bars"></i></a>
    </div>
  </div>
</div>
<div class="pages">
  <!-- Page, data-page contains page name-->
  <div data-page="services" class="page">
    <!-- Scrollable page content-->
    <div class="page-content">
      <div class="list-block">
         <ul>
                <?php 
                foreach ($files1 as $file)
                {
                	if (is_dir($dir.'/'.$file) && $file != '.' && $file != '..') {
                		$projects[] = $file;
                		?>
                		<li><a href="#" class="item-link">
	                      <div class="item-content">
	                        <div class="item-inner"> 
	                          <div class="item-title"><?=$file;?></div>
	                        </div>
	                      </div></a></li>
                		<?
                	}
                }
                ?>
          </ul>
       </div>
    </div>
  </div>
</div>