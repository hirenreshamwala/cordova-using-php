<?php 
include 'lib/cordova.class.php';

$dir    = 'Projects';
$files1 = scandir($dir);
?>


<!-- We don't need full layout here, because this page will be parsed with Ajax-->
<!-- Top Navbar-->
<div class="navbar">
  <div class="navbar-inner">
    <div class="left">
      <!-- left link contains only icon - additional "icon-only" class--><a href="#" class="link icon-only open-panel"> <i class="icon icon-bars"></i></a>
    </div><div class="center sliding">Projects</div>
    <div class="right">
      <a href="#" id="addProject" class="link icon-only"> <i class="icon icon-plus">+</i></a>
    </div>
  </div>
</div>
<div class="pages navbar-through toolbar-through">
  <div data-page="swipe-delete" class="page">
    <!-- Scrollable page content-->
    <div class="page-content">
      <div class="list-block">
         <ul>
                <?php 
                if(count($files1) > 0) {
	                foreach ($files1 as $file)
	                {
	                	if (is_dir($dir.'/'.$file) && $file != '.' && $file != '..') {
	                		?>
	                		<li class="swipeout item-link"  data-project="<?=$file?>" data-type="remove_project"><a href="#" class="view_project" data-id="<?=$file;?>">
		                      <div class="item-content swipeout-content">
		                        <div class="item-inner"> 
		                          <div class="item-title"><?=$file;?></div>
		                        </div>
		                      </div></a><div class="swipeout-actions-right"><a href="#" data-confirm="Are you sure you want to delete this item?" class="swipeout-delete">Delete</a></div></li>
	                		<?
	                	}
	                }
	            }
                ?>
          </ul>
       </div>
    </div>
  </div>
</div>