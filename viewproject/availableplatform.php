<?php 

include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
$availablePlatforms = array();
if ($projectName != '') {
	$cordova = new Cordova($projectName);
	$availablePlatforms = $cordova->availablePlatforms();
}
?>


<div class="navbar">
  <div class="navbar-inner">
    <div class="left sliding"><a href="#" class="back link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Project: <?=$projectName;?></div>
  	<div class="right">
      <a href="#" id="platform_add" class="link icon-only"> <i class="icon">Add</i></a>
    </div>
  </div>
</div>
<div class="pages navbar-through">
  <div data-page="forms-checkboxes" class="page">
    <div class="page-content">
      <div class="content-block-title">Available Platforms</div>
      <div class="list-block">
      <form id="availableplatforms" class="store-data list-block">
        <ul>
        	<?php if(count($availablePlatforms) > 0) {
        		foreach ($availablePlatforms as $platform){
        	?>
          <li>
            <label class="label-checkbox item-content">
              <input type="checkbox" name="ks-checkbox" value="<?=$platform;?>"  />
              <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
              <div class="item-inner">
                <div class="item-title"><?=$platform;?></div>
              </div>
            </label>
          </li>
          <?php }
			} else { ?>
          <div class="content-block-title">No platform available</div>
          <?php } ?>
        </ul>
        </form>
      </div>
    </div>
  </div>
</div>