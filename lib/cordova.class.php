<?php
//For MAC
putenv('PATH='. getenv('PATH') .':/usr/local/bin:/Users/Hiren/android-sdks/tools:/Users/Hiren/android-sdks/platform-tools');

class Cordova
{
	public $_ProjectName,
			$_NameSpace,
			$_ProjectDirectory;
	
	function __construct($projectname)
	{
		$this->_ProjectName = $projectname;
		$this->_NameSpace = 'com.xiontechnologies';
		$this->_ProjectDirectory = PROJECT_PATH;
		//$this->_ProjectDirectory = 'Projects';
	}
	
	/*
	 *	build()
	 *	It build the current project
	 *
	 *	@params: $release - Boolean (Default false)
	 *
	 *	Return: 0 - Build fail, 1 - Build success, 2 - No platform added
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function build($release = false)
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		
		$cmd = 'cordova build';
		$cmd .= ($release) ? ' --release' : '';
		$returnString = exec($cmd.' 2>&1',$output);
		
		$buildStatus = 0;
		if(strpos($returnString,'No platforms added') !== FALSE) {
			$buildStatus = 2;
			echo $returnString;
		}
		
		$reversed = array_reverse($output);
		if(is_array($reversed)) {
			foreach($reversed as $row) {
				if(strpos($row,'BUILD SUCCESSFUL') !== FALSE) {
					$buildStatus = 1;
				}
			}
		}
		
// 		echo '<pre>';
// 		print_r($output);
		chdir('../../');
		
		return $buildStatus;
	}
	
	/*
	*	run()
	*	It run the current project to emulator or device
	*
	*	Return: 0 - Build fail, 1 - Build success, 2 - No platform added
	*	Author: hiren.reshamwala@gmail.com
	*/
	function run()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
	
		$cmd = 'cordova run';
		$returnString = exec($cmd.' 2>&1',$output);
	
		$buildStatus = 0;
		if(strpos($returnString,'No platforms added') !== FALSE) {
			$buildStatus = 2;
			echo $returnString;
		}
		
		$reversed = array_reverse($output);
		if(is_array($reversed)) {
			foreach($reversed as $row) {
				if(strpos($row,'LAUNCH SUCCESS') !== FALSE) {
					$buildStatus = 1;
				}
			}
		}
	
// 		echo '<pre>';
// 		print_r($output);
		chdir('../../');
	
		return true;
	}
	
	/*
	 *	createProject()
	 *	It will create a cordova project
	 *
	 *	Return: 0 - Project unable to create, 1 - Project created successfully, 2 - Project already exists
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function createProject()
	{
		chdir($this->_ProjectDirectory);
		$createProject = 'cordova create '.$this->_ProjectName.' '.$this->_NameSpace.'.'.$this->_ProjectName.' '.$this->_ProjectName.' 2>&1';
		
		$output = exec($createProject);

		$isCreated = 0;
		if(strpos($output, 'Creating a new cordova project with name') !== FALSE)
		{
			$isCreated = 1;
		} else if(strpos($output, 'Path already exists') !== FALSE) {
			$isCreated = 2;
		}
		chdir('../');
		return $isCreated;
	}
	
	/*
	 *	addPlatform()
	 *	It will add platform to project
	 *
	 *	Parameter: $platform - android, ios, wp7, wp8, blackberry10
	 *	Return: 0 - Platform unable to add, 1 - Platform added successfully, 2 - Platform already added
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function addPlatform($platform)
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova platform add '.$platform.' 2>&1',$output);
		
		$isPlatformAdded = 0;
		if(is_array($output))
		{
			foreach($output as $row) {
				
				if(strpos($row, 'Platform android already added') !== FALSE) {
					$isPlatformAdded = 2;
					break;
				}
				
				if(strpos($row, 'Project successfully created') !== FALSE) {
					$isPlatformAdded = 1;
					break;
				}
			}
		}
		
		chdir('../../');
		
		return $isPlatformAdded;
	}
	
	/*
	 *	removePlatform()
	 *	It will remove platform from project
	 *
	 *	Parameter: $platform - android, ios, wp7, wp8, blackberry10
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function removePlatform($platform)
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova platform rm '.$platform.' 2>&1',$output);
		
		chdir('../../');
	}
	
	/*
	 *	availablePlatforms()
	 *	This function gives available platform for cordova
	 *
	 *	Return: Array()
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function availablePlatforms()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova platform ls 2>&1',$output);
		
		$availablePlatforms = array();
		if(is_array($output))
		{
			foreach($output as $row) {
				if(strpos($row, 'Available platforms:') !== FALSE) {
					
					$row = str_replace('Available platforms:','',$row);
					$availablePlatforms = explode(',',$row);
					if($row != '') {
						$availablePlatforms = explode(',',$row);
						foreach($availablePlatforms as $key=>$platform) {
							$availablePlatforms[$key] = trim($platform);
						}
					}
					break;
				}
			}
		}
		
		chdir('../../');
		return $availablePlatforms;
	}
	
	
	/*
	 *	installedPlatforms()
	 *	This function gives installed platform for cordova
	 *
	 *	Return: Array()
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function installedPlatforms()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova platform ls 2>&1',$output);
		
		$installedPlatforms = array();
		if(is_array($output))
		{
			foreach($output as $row) {
				if(strpos($row, 'Installed platforms:') !== FALSE) {
					
					$row = str_replace('Installed platforms:','',$row);
					if($row != '') {
						$installedPlatforms = explode(',',$row);
						foreach($installedPlatforms as $key=>$platform) {
							$installedPlatforms[$key] = trim($platform);
						}
					}
					break;
				}
			}
		}
		
		chdir('../../');
		return $installedPlatforms;
	}
	
	
	/*
	 *	addPlugin()
	 *	It will add plugin to project
	 *
	 *	Parameter: $plugin - name of the plugin
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function addPlugin($plugin)
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova plugin add '.$plugin.' 2>&1',$output);
		
		echo '<pre>';
		print_r($output);die;
		
		
		
		chdir('../../');
	}
	
	
	/*
	 *	addPlugin()
	*	It will add plugin to project
	*
	*	Parameter: $plugin - name of the plugin
	*	Author: hiren.reshamwala@gmail.com
	*/
	function searchPlugin($plugin)
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		exec('cordova plugin search '.$plugin.' 2>&1',$output);
	
		echo '<pre>';
		print_r($output);die;
	
	
	
		chdir('../../');
	}
	
	
	/*
	 *	generateKeyStore()
	 *	It will generate keystore for sign package
	 *
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function generateKeyStore()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		
		$keyToolCommand = 'keytool ';
		$keyToolCommand .= ' -genkeypair -dname "CN=Hiren, OU=com, O=xiontechnologies, L=surat, ST=gujarat, C=IN" ';
		$keyToolCommand .= ' -genkey -v ';
		$keyToolCommand .= ' -keystore '.$this->_ProjectName.'-release-key.keystore ';
		$keyToolCommand .= ' -alias '.$this->_ProjectName.' ';
		$keyToolCommand .= ' -keyalg RSA ';
		$keyToolCommand .= ' -keysize 2048  ';
		$keyToolCommand .= ' -storepass xiontechnologies ';
		$keyToolCommand .= ' -validity 9125 ';
	
		exec($keyToolCommand.'2>&1',$output);
		//exec('keytool -genkeypair -dname "CN=Hiren, OU=com, O=xiontechnologies, L=surat, ST=gujarat, C=IN" -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -storepass ab987c -validity 180 2>&1',$output);
		
// 		echo '<pre>';
// 		print_r($output);
		
		foreach($output as $row)
		{
			if(strpos($row, 'already exists') !== FALSE) {
				chdir('../../');
				return 'File already exists';
				break;
			}
		}
		
		chdir('../../');
		return 'Created';
	}
	
	/*
	 *	assignsign()
	 *	This function will assign generated keystore to unsigned apk
	 *	and make signed apk for release
	 *
	 *	Author: hiren.reshamwala@gmail.com
	 */
	function assignsign()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
		
		$command = 'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore '.$this->_ProjectName.'-release-key.keystore -storepass xiontechnologies '.$this->_ProjectName.'.apk '.$this->_ProjectName;
		echo $command;die;
		exec($command.' 2>&1',$output);
		
		echo '<pre>';
		print_r($output);
		
		chdir('../../');
	}
	
	/*
	 *	verifyapk()
	*	This function will assign generated keystore to unsigned apk
	*	and make signed apk for release
	*
	*	Author: hiren.reshamwala@gmail.com
	*/
	function verifyapk()
	{
		chdir($this->_ProjectDirectory);
		chdir($this->_ProjectName);
	
		$command = 'jarsigner -verify -verbose -certs '.$this->_ProjectName.'.apk';
		echo $command;die;
		exec($command.' 2>&1',$output);
	
		echo '<pre>';
		print_r($output);
	
		chdir('../../');
	}
	
	/*
	 *	alignapk()
	*	This function will assign generated keystore to unsigned apk
	*	and make signed apk for release
	*
	*	Author: hiren.reshamwala@gmail.com
	*/
	function alignapk()
	{
		//zipalign -v 4 your_project_name-unaligned.apk your_project_name.apk
	}
}
?>
