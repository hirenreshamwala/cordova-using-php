<?php 
include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
if ($projectName != '') {
	$cordova = new Cordova($projectName);
}
?>


<div class="navbar">
  <div class="navbar-inner">
    <div class="left sliding"><a href="#" class="back link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Create Project</div>
  	<div class="right">
      <a href="#" id="addProjectStep2" class="link icon-only"> <i class="icon">Submit</i></a>
    </div>
  </div>
</div>
<div class="pages navbar-through">
  <div data-page="form-storage" class="page">
    <div class="page-content">
      <div class="content-block-title">STEP 1</div>
      <div id="demoform-1" class="store-data list-block">
        <ul>
          <li>
            <div class="item-content">
              <div class="item-inner"> 
                <div class="item-title label">Project Name</div>
                <div class="item-input">
                  <input type="text" id="project_name" placeholder="Project name" name="name" required />
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>