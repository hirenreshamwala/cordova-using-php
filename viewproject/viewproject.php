<?php 

include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';

$installedPlatforms = array();
if ($projectName != '') {
	$cordova = new Cordova($projectName);
	$installedPlatforms = $cordova->installedPlatforms();
}
?>


<div class="navbar">
  <div class="navbar-inner">
    <div class="left sliding"><a href="#" class="projectback link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Project: <?=$projectName;?></div>
    <input type="hidden" id="project" value="<?=$projectName;?>" />
  </div>
</div>
<div class="pages navbar-through">
  <div data-page="swipe-delete" class="page">
    <div class="page-content">
      <div class="content-block-title">Platforms <a href="#" id="addPlatform"><i class="icon icon-plus">+</i></a></div>
      <div class="list-block">
        <ul>
        	<?php if(count($installedPlatforms) > 0) {
        		foreach ($installedPlatforms as $platform){
        	?>
          <li class="swipeout view_platform" data-id="<?=$platform?>" data-project="<?=$projectName?>" >
            <div class="item-content swipeout-content">
              <div class="item-inner"> 
                <div class="item-title"><?=$platform?></div>
              </div>
            </div>
            <div class="swipeout-actions-right"><a href="#" data-confirm="Are you sure you want to delete this item?" class="swipeout-delete">Delete</a></div>
          </li>
          <?php }
			} else { ?>
          <li class="content-block-title">No platform available</li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>