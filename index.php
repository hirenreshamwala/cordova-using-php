<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>Create App</title>
    <!-- Path to Framework7 Library CSS-->
    <link rel="stylesheet" href="public/css/framework7.min.css">
    <link rel="stylesheet" href="public/css/framework7.themes.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="public/css/app.css">
  </head>
  <body>
    <!-- Status bar overlay for fullscreen mode-->
    <div class="statusbar-overlay"></div>
    <!-- Panels overlay-->
    <div class="panel-overlay"></div>
    <!-- Left panel with reveal effect-->
    <div class="panel panel-left panel-reveal layout-dark">
      <div class="content-block-title">Menu</div>
      <div class="list-block">
        <ul>
          <li><a href="#" data-url="projects.php" class="with-animation close-panel item-link"> 
              <div class="item-content">
                <div class="item-media"><i class="icon icon-f7"></i></div>
                <div class="item-inner"> 
                  <div class="item-title">Projects</div>
                </div>
              </div></a>
              </form>
          
        </ul>
      </div>
    </div>
    <!-- Views-->
    <div class="views">
      <!-- Your main view, should have "view-main" class-->
      <div class="view view-main">
        <?php include 'projects.php'; ?>
      </div>
    </div>
    <script type="text/javascript" src="public/js/jquery-1.11.2.min.js"></script>
    <!-- Path to Framework7 Library JS-->
    <script type="text/javascript" src="public/js/framework7.min.js"></script>
    <!-- Path to your app js-->
    <script type="text/javascript" src="public/js/my-app.js"></script>
  </body>
</html>