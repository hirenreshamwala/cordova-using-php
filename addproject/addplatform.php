<?php
include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
$platforms = (array_key_exists('platforms', $_POST)) ? $_POST['platforms'] : array();

if (trim($projectName) != '') {
	$cordova = new Cordova($projectName);
	if (count($platforms) > 0) {
		foreach ($platforms as $platform)
		{
			$cordova->addPlatform($platform);
		}
		
		echo json_encode(array(
				'Type' => 'Success',
				'Message' => ''
		));die;
	} else {
		echo json_encode(array(
				'Type' => 'Error',
				'Message' => 'Please select platform'
		));die;
	}
}

echo json_encode(array(
		'Type' => 'Error',
		'Message' => 'Something goes wrong. Please contact administrator.'
));die;