<?php 

include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
$platforms = (array_key_exists('platforms', $_POST)) ? $_POST['platforms'] : array();

if ($projectName != '') {
	$cordova = new Cordova($projectName);
	if (count($platforms) > 0) {
		foreach ($platforms as $platform) {
			$cordova->addPlatform($platform);
		}
	}
}
?>