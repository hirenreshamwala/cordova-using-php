<?php

if(count($_FILES) > 0)
{
	if ($_FILES["file"]["error"] > 0) {
	  echo "Error: " . $_FILES["file"]["error"] . "<br>";
	} else {
		$allowedExts = array("zip");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		
		if (($_FILES["file"]["type"] == "application/octet-stream")
		&& in_array($extension, $allowedExts)) {
		  if ($_FILES["file"]["error"] > 0) {
			echo "Error: " . $_FILES["file"]["error"] . "<br>";
		  } else {
		  
			$zip = zip_open($_FILES["file"]["tmp_name"]);
			if ($zip) {
				while ($zip_entry = zip_read($zip)) {
				echo "Name:               " . zip_entry_name($zip_entry) . "<br>";
				echo "Actual Filesize:    " . zip_entry_filesize($zip_entry) . "<br>";
				echo "Compressed Size:    " . zip_entry_compressedsize($zip_entry) . "<br>";
				echo "Compression Method: " . zip_entry_compressionmethod($zip_entry) . "<br>";
					if (zip_entry_open($zip, $zip_entry, "r")) {
						echo "File Contents:<br>";
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						//echo "$buf";
						zip_entry_close($zip_entry);
					}
					echo "<br><br><br>";
				}
				zip_close($zip);
			}

			echo "Upload: " . $_FILES["file"]["name"] . "<br>";
			echo "Type: " . $_FILES["file"]["type"] . "<br>";
			echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
			echo "Stored in: " . $_FILES["file"]["tmp_name"];
		  }
		} else {
		  echo "Invalid file";
		}
	}
}
?>

<html>
<body>

<form action="upload.php" method="post"
enctype="multipart/form-data">
<label for="file">Filename:</label>
<input type="file" name="file" id="file"><br>
<input type="submit" name="submit" value="Submit">
</form>

</body>
</html>