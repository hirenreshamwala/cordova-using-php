<?php 

include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';

$installedPlatforms = array();
if ($projectName != '') {
	$cordova = new Cordova($projectName);
	
}
?>


<div class="navbar">
  <div class="navbar-inner">
    <div class="left sliding"><a href="#" class="projectback link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Android Project: <?=$projectName;?></div>
    <input type="hidden" id="project" value="<?=$projectName;?>" />
    <div class="right"><a href="#" data-project="<?=$projectName;?>" class="projectbuild link"> Build</a></div>
    
  </div>
</div>
<div class="pages navbar-through">
  <div data-page="swipe-delete" class="page">
    <div class="page-content">
    	<button class="button" data-project="<?=$projectName;?>" id="build_android_download" >Build and download</button>
    	<button class="button" data-project="<?=$projectName;?>" id="android_download" >download</button>
      <button class="button" data-project="<?=$projectName;?>" data-type="dev" id="generate_android_keystore" >Generate Keystore Development</button>
      <button class="button" data-project="<?=$projectName;?>" data-type="release" id="generate_android_keystore" >Generate Keystore Release</button>
      <button class="button" data-project="<?=$projectName;?>" id="run_android_project" >Run</button>
    </div>
  </div>
</div>