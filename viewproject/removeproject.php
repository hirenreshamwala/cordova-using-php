<?php
include '../config.php';
$projectName = (array_key_exists ( 'projectname', $_POST )) ? $_POST ['projectname'] : '';
rrmdir('../Projects/'.$projectName);

function rrmdir($dir) {
	
	if (is_dir ( $dir )) {
		$objects = scandir ( $dir );
		foreach ( $objects as $object ) {
			if ($object != "." && $object != "..") {
				if (filetype ( $dir . "/" . $object ) == "dir")
					rrmdir ( $dir . "/" . $object );
				else
					unlink ( $dir . "/" . $object );
			}
		}
		reset ( $objects );
		rmdir ( $dir );
	}
}