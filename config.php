<?php 
defined('BASE_PATH')
	|| define('BASE_PATH', realpath(dirname(__FILE__)));
set_include_path(implode(PATH_SEPARATOR, array(
	realpath(BASE_PATH),
	BASE_PATH,
	BASE_PATH . "/lib",
	get_include_path(),
)));
define('PROJECT_PATH', BASE_PATH . "/Projects/");

?>