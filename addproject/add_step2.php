<?php 
include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
if ($projectName != '') {
	$cordova = new Cordova($projectName);
	$availablePlatforms = $cordova->availablePlatforms();
}
?>


<div class="navbar">
  <div class="navbar-inner">
    <div class="left sliding"><a href="#" class="back link"> <i class="icon icon-back"></i><span>Back</span></a></div>
    <div class="center sliding">Create Project</div>
  	<div class="right">
      <a href="#" id="addProjectStep3" class="link icon-only"> <i class="icon">Submit</i></a>
    </div>
  </div>
</div>
<div class="pages navbar-through">
  <div data-page="form-storage" class="page">
    <div class="page-content">
      <div class="content-block-title">STEP 2 - Select platform</div>
      <form id="step2form" class="store-data list-block">
        <ul>
        	<?php 
        		if (count($availablePlatforms) > 0) {
        			foreach ($availablePlatforms as $platform) {
        			?>
					<li>
						<label class="label-checkbox item-content">
							<input type="checkbox" name="ks-checkbox" value="<?=$platform?>"/>
							<div class="item-media"><i class="icon icon-form-checkbox"></i></div>
							<div class="item-inner">
								<div class="item-title"><?=$platform;?></div>
							</div>
						</label>
					</li>	
					<?
        			}
        		}
        	?>
        </ul>
      </form>
    </div>
  </div>
</div>