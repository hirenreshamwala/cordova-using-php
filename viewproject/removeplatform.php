<?php 
include '../config.php';
include 'cordova.class.php';
$projectName = (array_key_exists('projectname', $_POST)) ? $_POST['projectname'] : '';
$platform = (array_key_exists('platform', $_POST)) ? $_POST['platform'] : '';


if ($projectName != '' && $platform != '') {
	$cordova = new Cordova($projectName);
	$platform = current(explode(' ', $platform));
	$cordova->removePlatform($platform);
}

?>